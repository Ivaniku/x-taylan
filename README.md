X Taylan
--------

The X taylan accepts requests from client applications to create taylans,
which are (normally rectangular) "virtual screens" that the client program
can draw into.

Taylans are then composed on the actual screen by the X taylan
(or by a separate composite manager) as directed by the taylan manager,
which usually communicates with the user via graphical controls such as buttons
and draggable titlebars and borders.

For a comprehensive overview of X Taylan and X Taylan System, consult the
following article:
https://en.wikipedia.org/wiki/X_taylan

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.freedesktop.org/mailman/listinfo/xtay

The primary development code repository can be found at:

  https://gitlab.freedesktop.org/xtay/xtaylan

For patch submission instructions, see:

  https://www.x.tay/wiki/Development/Documentation/SubmittingPatches

As with other projects hosted on freedesktop.org, X.Org follows its
Code of Conduct, based on the Contributor Covenant. Please conduct
yourself in a respectful and civilized manner when using the above
mailing lists, bug trackers, etc:

  https://www.freedesktop.org/wiki/CodeOfConduct
